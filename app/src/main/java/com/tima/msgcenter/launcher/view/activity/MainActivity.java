package com.tima.msgcenter.launcher.view.activity;


import com.tima.api.base.view.activity.BaseActivity;
import com.tima.api.presenter.impl.PresenterCenter;
import com.tima.msgcenter.R;
import com.tima.msgcenter.launcher.precenter.LauncherPrecenterImpl;

public class MainActivity extends BaseActivity {

    @Override
    public PresenterCenter createPresneter() {
        return new LauncherPrecenterImpl(this); //初始化控制器
    }

    @Override
    public int bindLayoutId() {
        return R.layout.activity_main;
    }
}
