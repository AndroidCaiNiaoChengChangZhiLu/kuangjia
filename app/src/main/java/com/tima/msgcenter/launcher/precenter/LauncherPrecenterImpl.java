package com.tima.msgcenter.launcher.precenter;

import android.content.Context;

import com.tima.api.base.model.BaseModel;
import com.tima.api.base.presenter.BasePresenter;
import com.tima.msgcenter.launcher.model.LauncherModelImpl;


/**
 * Created by Administrator on 2018/1/3.
 */


public class LauncherPrecenterImpl extends BasePresenter {
    private Context context;
    public LauncherPrecenterImpl(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public BaseModel bindModel() {
        return new LauncherModelImpl(context);
    }
}
