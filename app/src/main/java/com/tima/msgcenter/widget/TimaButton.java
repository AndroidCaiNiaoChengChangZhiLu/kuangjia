package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaButton extends android.support.v7.widget.AppCompatButton {
    public TimaButton(Context context) {
        super(context);
    }

    public TimaButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
