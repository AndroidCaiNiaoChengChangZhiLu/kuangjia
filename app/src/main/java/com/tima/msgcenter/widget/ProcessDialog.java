package com.tima.msgcenter.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tima.msgcenter.R;


public class ProcessDialog extends AlertDialog implements View.OnClickListener {

    private Button mRefreshCloseBtn;
    private FrameLayout mRefreshRelativeLayout;
    private ImageView mRefreshImageView;
    private TextView mRefreshTextView;
    private Context mContext;
    private Animation opertionAnim = null;

    public ProcessDialog(Context context) {
        super(context);

        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.process);
        initView();
    }

    public void initView() {
        mRefreshRelativeLayout = (FrameLayout) findViewById(R.id.reshRelativeLayout);
        mRefreshImageView = (ImageView) findViewById(R.id.refresh_ImageView);
        mRefreshTextView = (TextView) findViewById(R.id.refresh_TextView);
        mRefreshCloseBtn = (Button) findViewById(R.id.close_ImageView);
        mRefreshRelativeLayout.setVisibility(View.GONE);
        mRefreshRelativeLayout.setOnClickListener(this);
        mRefreshCloseBtn.setOnClickListener(this);

        //动画效果初始化
        opertionAnim = AnimationUtils.loadAnimation(mContext, R.anim.tip);
        LinearInterpolator lin = new LinearInterpolator();
        opertionAnim.setInterpolator(lin);
    }

    @Override
    public void onClick(View view) {
        int nId = view.getId();
        switch (nId) {
            case R.id.close_ImageView:
                stopAnim();
                cancel();
                break;
        }
    }

    public boolean isShow() {
        if (mRefreshRelativeLayout.getVisibility() == View.VISIBLE) {
            return true;
        }
        return false;
    }
    public void showDialog(String str){

        this.show();
        this.setCanceledOnTouchOutside(false);
        startAnim(str);

    }
    public void hineDialog( ){
        this.cancel();
    }
    public void startAnim(String str) {
        mRefreshRelativeLayout.setVisibility(View.VISIBLE);
        mRefreshTextView.setText(str);
        if (opertionAnim != null) {
            mRefreshImageView.startAnimation(opertionAnim);
        }
    }

    public void stopAnim() {
        mRefreshRelativeLayout.setVisibility(View.GONE);
        if (opertionAnim != null) {
            mRefreshImageView.clearAnimation();
            opertionAnim = null;
        }
    }
}
