package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaGridView extends GridView {
    public TimaGridView(Context context) {
        super(context);
    }

    public TimaGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
