package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaImageView extends android.support.v7.widget.AppCompatImageView {
    public TimaImageView(Context context) {
        super(context);
    }

    public TimaImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
