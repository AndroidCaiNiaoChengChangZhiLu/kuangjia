package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaImageButton extends android.support.v7.widget.AppCompatImageButton {
    public TimaImageButton(Context context) {
        super(context);
    }

    public TimaImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
