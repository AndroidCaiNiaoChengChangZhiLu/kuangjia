package com.tima.msgcenter.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaLinearLayout extends LinearLayout {
    public TimaLinearLayout(Context context) {
        super(context);
    }

    public TimaLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
