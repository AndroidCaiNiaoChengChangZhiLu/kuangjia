package com.tima.msgcenter.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.tima.msgcenter.R;


/**
 * Created by Administrator on 2017/10/11.
 */

public class HintDialog {
    private static HintDialog mHintDialog;

    private HintDialog() {
    }

    public static synchronized HintDialog getInstance() {
        if (mHintDialog == null) {
            mHintDialog = new HintDialog();
        }
        return mHintDialog;
    }

    public void showNoticeDialog(String appPath, Context context) {
        final AlertDialog myDialog = new AlertDialog.Builder(context).create();
        myDialog.setCanceledOnTouchOutside(false);// 设置点击屏幕Dialog不消失
        myDialog.setCancelable(false);
        myDialog.show();
        myDialog.getWindow().setContentView(R.layout.timadialog);
        TextView explain_mydialog = (TextView) myDialog.getWindow().findViewById(R.id.explain_mydialog);
        explain_mydialog.setText(appPath);
        TextView reset_mydialog = (TextView) myDialog.getWindow().findViewById(R.id.reset_mydialog);
        myDialog.getWindow()
                .findViewById(R.id.button_back_mydialog)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                        close();
                    }
                });
        reset_mydialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    public void close() {
        System.exit(0);
    }
}
