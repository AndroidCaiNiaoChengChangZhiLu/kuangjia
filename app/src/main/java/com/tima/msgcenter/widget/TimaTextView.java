package com.tima.msgcenter.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaTextView extends android.support.v7.widget.AppCompatTextView {
    public TimaTextView(Context context) {
        super(context);
    }

    public TimaTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
