package com.tima.msgcenter.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaRecyclerView extends RecyclerView {
    public TimaRecyclerView(Context context) {
        super(context);
    }

    public TimaRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
