package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaListView extends ListView {
    public TimaListView(Context context) {
        super(context);
    }

    public TimaListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
