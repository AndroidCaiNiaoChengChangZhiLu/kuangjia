package com.tima.msgcenter.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Administrator on 2018/1/30.
 */

public class TimaRelativeLayout extends RelativeLayout {
    public TimaRelativeLayout(Context context) {
        super(context);
    }

    public TimaRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimaRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
