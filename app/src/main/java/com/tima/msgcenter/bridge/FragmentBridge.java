package com.tima.msgcenter.bridge;

import com.tima.api.base.view.fragment.BaseFragment;

/**
 * Created by Administrator on 2018/1/24.
 */

public abstract class FragmentBridge {
    abstract void addFragment();
    abstract void remove();
    abstract void replace();
    abstract void hine();
    abstract void show();
    abstract void init(BaseFragment fragmentMaseger);
}
