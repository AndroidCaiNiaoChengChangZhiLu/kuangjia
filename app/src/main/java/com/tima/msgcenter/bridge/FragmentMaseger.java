package com.tima.msgcenter.bridge;

import android.support.v4.app.FragmentActivity;

import com.tima.api.base.view.fragment.BaseFragment;

/**
 * Created by Administrator on 2018/1/24.
 */
public abstract class FragmentMaseger {
    abstract void addFragment(BaseFragment fragment);
    abstract void remove(BaseFragment fragment);
    abstract void replace(BaseFragment fragment);
    abstract void hine(BaseFragment fragment);
    abstract void show(BaseFragment fragment);
    abstract FragmentMasegerImpl init(FragmentActivity activity);
}
