package com.tima.msgcenter.bridge;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.tima.api.base.view.fragment.BaseFragment;

/**
 * Created by Administrator on 2018/1/24.
 */

public class FragmentMasegerImpl extends FragmentMaseger {

    private static FragmentMasegerImpl mFragmentMasegerImpl;
    private FragmentTransaction fragmentTransaction;
    private FragmentActivity activity;

    private FragmentMasegerImpl() {
    }

    public static synchronized FragmentMasegerImpl getInstance() {
        if (mFragmentMasegerImpl == null) {
            mFragmentMasegerImpl = new FragmentMasegerImpl();
        }
        return mFragmentMasegerImpl;
    }


    @Override
    void addFragment(BaseFragment fragment) {
        fragmentTransaction.add(0, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    @Override
    void remove(BaseFragment fragment) {
        fragmentTransaction.remove(fragment).disallowAddToBackStack().commitAllowingStateLoss();
    }

    @Override
    void replace(BaseFragment fragment) {
        fragmentTransaction .replace(0, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    @Override
    void hine(BaseFragment fragment) {
        if (fragment != null) {
            fragmentTransaction.hide(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    void show(BaseFragment fragment) {
        if (fragment != null) {
            fragmentTransaction.show(fragment).commitAllowingStateLoss();
        }
    }


    @Override
    FragmentMasegerImpl init(FragmentActivity activity) {
        this.activity = activity;
        fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        return mFragmentMasegerImpl;
    }
}
