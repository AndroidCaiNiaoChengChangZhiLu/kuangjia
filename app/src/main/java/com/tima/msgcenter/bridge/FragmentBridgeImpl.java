package com.tima.msgcenter.bridge;

import android.support.v4.app.FragmentActivity;

import com.tima.api.base.view.fragment.BaseFragment;

/**
 * Created by Administrator on 2018/1/24.
 */

public class FragmentBridgeImpl extends FragmentBridge{
    private BaseFragment fragment;
    private FragmentMaseger fragmentMaseger;

    public FragmentBridgeImpl(FragmentActivity fragmentActivity) {
        fragmentMaseger = FragmentMasegerImpl.getInstance().init(fragmentActivity);//初始化Fragment管理器
    }

    @Override
    void addFragment() {
        fragmentMaseger.addFragment(fragment);
    }

    @Override
    void remove() {
        fragmentMaseger.remove(fragment);
    }

    @Override
    void replace() {
        fragmentMaseger.replace(fragment);
    }

    @Override
    void hine() {
        fragmentMaseger.hine(fragment);
    }

    @Override
    void show() {
        fragmentMaseger.show(fragment);
    }

    @Override
    void init(BaseFragment fragment) {
        this.fragment = fragment;
    }
}
