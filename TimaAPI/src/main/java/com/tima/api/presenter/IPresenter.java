package com.tima.api.presenter;

import com.tima.api.view.IUIFrame;

/**
 * Created by Administrator on 2018/1/3.
 */

public interface IPresenter<V extends IUIFrame> {
    //绑定视图
    public void attachView(V view);
    //接触绑定
    public void dettachView();
}
