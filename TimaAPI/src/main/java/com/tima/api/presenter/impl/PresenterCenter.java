package com.tima.api.presenter.impl;

import com.tima.api.presenter.IPresenter;
import com.tima.api.view.IUIFrame;

/**
 * Created by Administrator on 2018/1/3.
 */

public abstract class PresenterCenter<V extends IUIFrame> implements IPresenter<V> {
    private V view;
    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void dettachView() {
        this.view = null;
    }
}
