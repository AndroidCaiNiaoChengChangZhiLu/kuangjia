package com.tima.api.http.dataAnalysis.AnalysisFactory;


import com.tima.api.been.ModelBase;

/**
 * Created by Administrator on 2018/1/31.
 */

public interface IAnalysisFactory {
    public ModelBase Produce(String json);
//    public ModelBase BindPushProduce(String json);
//    public ModelBase DeleteMsgByIdProduce(String json);
//    public ModelBase MsgContentProduce(String json);
//    public ModelBase MsgListProduce(String json);
//    public ModelBase MsgNumProduce(String json);
//    public ModelBase MsgReadedProduce(String json);
//    public ModelBase SendPoiToMyMobileProduce(String json);
//    public ModelBase ShareLocationProduce(String json);
//    public ModelBase UnReadMsgListProduce(String json);

}
