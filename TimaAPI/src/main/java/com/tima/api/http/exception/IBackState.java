package com.tima.api.http.exception;


import com.tima.api.been.ModelBase;

/**
 * Created by Administrator on 2017/7/12.
 */

public interface IBackState<T> {
    /**
     * 判断返回json类型并反馈数据
     */
    void isJsonListOrObj(String responseBody, ModelBase modelBase);

    /**
     * 返回类型处理
     */
    void responseCode(int code, ModelBase modelBase);

    void responseErroCode(int erroCode, String erroMessage);
}
