package com.tima.api.http.dataAnalysis.AnalysisFactory.AnalysisFactoryImpl;

import com.tima.api.been.ModelBase;
import com.tima.api.http.dataAnalysis.AnalysisFactory.IAnalysisFactory;
import com.tima.api.http.dataAnalysis.dataAnalysisImpl.BindPushAnalysis;

/**
 * Created by Administrator on 2018/1/31.
 */

public class BindPushFactory implements IAnalysisFactory {
    @Override
    public ModelBase Produce(String json) {
        return new BindPushAnalysis().DataAnalysis(json);
    }
}
