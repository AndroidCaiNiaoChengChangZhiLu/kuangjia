package com.tima.api.http.utils;


import com.google.gson.Gson;
import com.tima.api.http.callback.TimaCallbackNet;
import com.tima.api.http.dataAnalysis.AnalysisFactory.IAnalysisFactory;
import com.tima.api.http.dataAnalysis.agency.FactoryAgency;
import com.tima.api.http.exception.impl.BackState;
import com.tima.api.been.ModelBase;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017/7/12.
 */

public class JsonType<T> {
    private static JsonType mJsonType;
    private static Gson gson;
    private static ArrayList arrayObj;

    private JsonType() {
        arrayObj = new ArrayList();
        gson = new Gson();
    }

    public static synchronized JsonType getJsonType() {
        if (mJsonType == null) {
            mJsonType = new JsonType();
        }
        return mJsonType;
    }


    public <T> void parseCommonJson(String responseBody, TimaCallbackNet tTimaCallbackNet, final ModelBase modelBase, BackState backState) {
        try {
             if (isJsonEmpty(responseBody)) {
                tTimaCallbackNet.onFailure("数据异常");
            }
            IAnalysisFactory mIAnalysisFactory = null;
            mIAnalysisFactory = (IAnalysisFactory) Class.forName(modelBase.getName()).newInstance();
            IAnalysisFactory bf = (IAnalysisFactory) FactoryAgency.factory(mIAnalysisFactory);
            tTimaCallbackNet.onSucceed(bf.Produce(responseBody));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    public static boolean isJsonEmpty(String data) {
        if (data == null || "".equals(data) || "[]".equals(data)
                || "{}".equals(data) || "null".equals(data)) {
            return true;
        }
        return false;
    }
}
