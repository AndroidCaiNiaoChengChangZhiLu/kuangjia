package com.tima.api.http.exception.impl;


import android.util.Log;

import com.tima.api.http.callback.TimaCallbackNet;
import com.tima.api.http.exception.IBackState;
import com.tima.api.http.utils.JsonType;
import com.tima.api.http.utils.ResponseCodeUtil;
import com.tima.api.been.ModelBase;


/**
 * Created by Administrator on 2017/7/12.
 * 返回类型处理业务类
 */

public class BackState implements IBackState {
    private TimaCallbackNet tTimaCallbackNet;

    public BackState(TimaCallbackNet tTimaCallbackNet) {
        this.tTimaCallbackNet = tTimaCallbackNet;
    }

    /**
     * 判断返回json类型并反馈数据
     */
    @Override
    public void isJsonListOrObj(String responseBody, ModelBase modelBase) {
        JsonType.getJsonType().parseCommonJson(responseBody, tTimaCallbackNet, modelBase, this);
    }

    /**
     * 返回类型处理
     */
    @Override
    public void responseCode(int code, ModelBase modelBase) {
        ResponseCodeUtil.getResponseCodeUtil().codeManage(code, tTimaCallbackNet);
    }

    /**
     * 返回类型处理
     */
    @Override
    public void responseErroCode(int erroCode, String erroMessage) {
        Log.e("zyr","-----------------------------------------erroCode="+erroCode+erroMessage);
        if (erroCode != 0) {
//
//            /**
//             * -1	缺少必要的参数或找不到车牌前缀所匹配的城市
//             -3  	本系统暂不提供该城市违章查询请求
//             -5   服务器错误（超时，数据获取异常等）
//
//             -10     未被授权访问该服务或用户名密码不正确
//             -20		未和错误
//             -40     未被授权查询此车牌信息
//             -41     输入参数不符合数据源要求
//             -42     数据源暂不可用
//             -43     当日查询数已达到授权数标准，无法继续查询
//
//             -6      错误：您输入信息有误
//             -61		输入车牌号有误
//             -62 		输入车架号有误
//             -63		输入发动机号有误
//             -66     不支持的车辆类型
//             -67		该省份（城市）不支持异地车牌
//             */
//
//            switch (erroCode){
//                case -1:
//                    erroMessage = "";
//                    break;
//                case -3:
//                    erroMessage = "";
//                    break;
//                case -5:
//                    erroMessage = "";
//                    break;
//                case -10:
//                    erroMessage = "";
//                    break;
//            }
            tTimaCallbackNet.onFailure(erroMessage);
        }
    }
}
