package com.tima.api.http.callback;


import com.squareup.okhttp.ResponseBody;
import com.tima.api.http.exception.impl.BackState;
import com.tima.api.been.ModelBase;

import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.Response;

/**
 * Created by Administrator on 2017/7/12.
 */

public abstract class TimaCallbackNet<T extends ResponseBody> implements Callback<T> {
    private BackState mBackState;
    private ModelBase modelBase;
    private String string = null;

    public TimaCallbackNet(ModelBase modelBase) {
        this.modelBase = modelBase;
        if (mBackState == null) {
            mBackState = new BackState(this);
        }
    }

    @Override
    public void onResponse(Response<T> response) {
        try {
            if (response.body() != null) {
                string = response.body().string();
            }
            mBackState.responseCode(response.code(), modelBase);  //返回code码处理
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        onFailure("未知错误");
    }

    /**
     * 处理服务器反馈数据
     */
    public void response() {
        if (string != null) {
            mBackState.isJsonListOrObj(string, modelBase);
        }
    }

    public abstract void onSucceed(ArrayList response);

    public abstract void onFailure(String message);

    public abstract void onAutoLogin();
}
