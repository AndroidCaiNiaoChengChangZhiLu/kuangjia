package com.tima.api.http.dataAnalysis.agency;

import com.tima.api.http.dataAnalysis.AnalysisFactory.IAnalysisFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by Administrator on 2018/1/31.
 */

public class FactoryAgency implements InvocationHandler {
    private Object obj;
    FactoryAgency() {}

    FactoryAgency(Object obj) {
     this.obj = obj;
   }
    public static Object factory(Object obj)
        {
                Class cls = obj.getClass();
                return Proxy.newProxyInstance(cls.getClassLoader(),cls.getInterfaces(),new FactoryAgency(obj));
        }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        result = method.invoke(obj,args);
        return result;
    }
}
