package com.tima.api.http.utils;


import com.tima.api.http.callback.TimaCallbackNet;

/**
 * Created by Administrator on 2017/7/12.
 * 服务器反馈code处理
 */

public class ResponseCodeUtil {
    private static ResponseCodeUtil mResponseCodeUtil;

    private ResponseCodeUtil() {
    }

    public static synchronized ResponseCodeUtil getResponseCodeUtil() {
        if (mResponseCodeUtil == null) {
            mResponseCodeUtil = new ResponseCodeUtil();
        }
        return mResponseCodeUtil;
    }

    public void codeManage(int code, TimaCallbackNet tTimaCallbackNet) {
        switch (code) {
            case 200:
                tTimaCallbackNet.response();
                break;
            default:
                tTimaCallbackNet.onFailure("请检查网络");
                break;
        }
    }

}
