package com.tima.api.http.request;


import android.util.Log;

import com.squareup.okhttp.ResponseBody;
import com.tima.api.http.OnDataCallback;
import com.tima.api.http.callback.TimaCallbackNet;
import com.tima.api.been.ModelBase;

import java.util.ArrayList;

import retrofit.Call;

/**
 * Created by Administrator on 2017/7/10.
 * 网络请求
 */

public class HttpExecute {
    private static HttpExecute mHttpExecute;

    private HttpExecute() {

    }

    public static synchronized HttpExecute getHttpExecute() {
        if (mHttpExecute == null) {
            mHttpExecute = new HttpExecute();
        }
        return mHttpExecute;
    }

    /**
     * 执行请求
     * @param call
     * @param callback
     * @param modelBase
     */
    public void executeCall(Call<ResponseBody> call, final OnDataCallback callback, ModelBase modelBase) {
        call.enqueue(new TimaCallbackNet<ResponseBody>(modelBase) {

            @Override
            public void onSucceed(ArrayList response) {
                Log.e("hbwang","======================httpexecuteCall.onSucceed>");
                callback.onSuccess(response);
            }

            @Override
            public void onFailure(String message) {
                Log.e("hbwang","======================httpexecuteCall.onFailure>"+message);
                callback.onError(message);
            }

            @Override
            public void onAutoLogin() {
            }
        });
    }
}
