package com.tima.api.base.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tima.api.presenter.impl.PresenterCenter;
import com.tima.api.view.IUICallBack;
import com.tima.api.view.IUIFrame;

/**
 * 框架类
 * @param <P>
 * @param <V>
 */

public abstract class BaseFragment<P extends PresenterCenter,V extends IUIFrame> extends FrameFragment<P,V> implements IUICallBack {

    private View contentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            contentView = inflater.inflate(bindLayoutId(),container,false);
        return contentView;
    }



    //fragment布局文件的id
    public abstract int bindLayoutId();

}
