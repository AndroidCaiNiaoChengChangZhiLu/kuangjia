package com.tima.api.base.presenter;

import android.content.Context;

import com.tima.api.base.model.BaseModel;
import com.tima.api.presenter.impl.PresenterCenter;
import com.tima.api.view.IUIFrame;

public abstract class BasePresenter<M extends BaseModel,V extends IUIFrame> extends PresenterCenter<V> {

    private Context context;

    private M model;

    public BasePresenter(Context context){
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public M getModel(){
        if (this.model == null){
            this.model = bindModel();
        }
        return this.model;
    }

    public abstract M bindModel();

}
