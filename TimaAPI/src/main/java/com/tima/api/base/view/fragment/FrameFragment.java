package com.tima.api.base.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.tima.api.presenter.impl.PresenterCenter;
import com.tima.api.view.IUIFrame;

/**
 * Created by Administrator on 2018/1/3.
 *
 */

public abstract class FrameFragment<P extends PresenterCenter,V extends IUIFrame> extends Fragment {
    private PresenterCenter presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.presenter  =  createPresneter();
        if (this.presenter != null){
            this.presenter.attachView(createView());
        }
    }

    public abstract P createPresneter();
    public abstract V createView();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.presenter != null){
            this.presenter.dettachView();
            this.presenter = null;
        }
    }
}
