package com.tima.api.base.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tima.api.presenter.impl.PresenterCenter;
import com.tima.api.view.IUIFrame;

/**
 * Created by Administrator on 2018/1/3.
 */

public abstract class FrameAvtivity<P extends PresenterCenter> extends AppCompatActivity implements IUIFrame {

    private P mPresneter; //给我们的P层绑定V
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( bindLayoutId());
        mPresneter = createPresneter();

        //绑定View
        if (this.mPresneter != null){
            this.mPresneter.attachView(this);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //解除绑定
        if (this.mPresneter != null){
            this.mPresneter.dettachView();
            this.mPresneter = null;
        }
    }
    //activity布局文件的id
    public abstract int bindLayoutId();
    //创建管理器
    public abstract P createPresneter();
}
