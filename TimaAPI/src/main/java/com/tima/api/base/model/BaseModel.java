package com.tima.api.base.model;

import android.content.Context;

import com.tima.api.model.IModel;


public class BaseModel implements IModel {

    private Context context;

    public BaseModel(Context context){
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
