package com.tima.api.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SignatureGenerator {

    /**
     * 閺嶈宓乁rl閿涘牐绁┃鎰板劥閸掑棴绱氶敍瀵�rl閸欏倹鏆熼崚妤勩�冮崪瀛瞖cretKey閻㈢喐鍨氱粵鎯ф倳
     *
     * @param urlResourcePart URL鐠у嫭绨柈銊ュ瀻閿涘奔绶ユ俊鍌涙箒婵″倷绗匲RL閿涙ttp://faw-vw.timanetwork.com/access/tm/user/getUserInfo?apramX=valueX, 閸忔儼绁┃鎰板劥閸掑棙妲�"user/getUserInfo"
     * @param params
     * @param secretKey
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    public static String generate(String urlResourcePart, Map<String, String> params, String secretKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        //鐎电懓寮弫鐗堝瘻閸氬秶袨閹烘帒绨�(閸楀洤绨�)
        List<Entry<String, String>> parameters = new LinkedList<Entry<String, String>>(params.entrySet());
        Collections.sort(parameters, new Comparator<Entry<String, String>>() {
            @Override
            public int compare(Entry<String, String> o1, Entry<String, String> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });

        //瑜般垺鍨氶崣鍌涙殶鐎涙顑佹稉锟�, 楠炶埖濡窼ecretKey閸旂姴婀張顐㈢啲閿涘澃alt閿涳拷
        StringBuilder sb = new StringBuilder();
        sb.append(urlResourcePart).append("_");
        for (Entry<String, String> param : parameters) {
            sb.append(param.getKey()).append("=").append(param.getValue()).append("_");
        }
        sb.append(secretKey);

        String baseString = URLEncoder.encode(sb.toString(), "UTF-8");
        return MD5Util.md5(baseString);
    }

    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Map<String, String> params = new HashMap<String, String>();


        params.put("vin", "");
        params.put("sids", "");
        params.put("token", "");
        params.put("appkey", "");
        String urlResourcePart = "";//接口URL：service-accounting/serviceAccounting/check
        String sign = generate(urlResourcePart, params, "");//secretkey值
        System.out.println(sign);

    }
}
