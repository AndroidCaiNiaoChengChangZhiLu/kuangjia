package com.tima.api.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tima.api.base.app.ApiApplication;
import com.tima.api.been.ModelTokey;
import com.tima.api.config.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;


/**
 * Created by Administrator on 2017/8/23.
 */

public class CarCodeNumTools {
    private static CarCodeNumTools  mCarCodeNum;
    static SharedPreferences mSharedPreferences;
    private Gson mGson;
    private Activity context;

    private CarCodeNumTools() {
        init();
    }
    public static synchronized CarCodeNumTools getInstance() {
        if (mCarCodeNum == null) {
            mCarCodeNum = new CarCodeNumTools();
        }
        return mCarCodeNum;
    }

    /**
     * 初始化获取车机五码数据
     */
    private void init() {
        try {
            mGson = new Gson();
            Context mContextToken = ApiApplication.getApplication().createPackageContext(Config.CACHE_PACKAGE, Context.CONTEXT_IGNORE_SECURITY);
            mSharedPreferences = mContextToken.getSharedPreferences(Config.CACHE_NAME, Config.TOKEN_MODE);//只读权限
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取Appkey
     */
    public String getAppkey() {
        return Config.Appkey;
    }
    /**
     * 获取Vin
     */
    public String getVin() {
        return Settings.System.getString(context.getContentResolver(),"tbox_vin_data");
    }
    /**
     * 获取Token
     */
    public String getToken() {
        ModelTokey modelTokey = null;
        try {
            modelTokey = getObject(Config.CACHE_KEY, ModelTokey.class);
            if (modelTokey == null) {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return modelTokey.getToken();
    }
    /**
     * 获取Aid
     */
    public String getAid() {
        ModelTokey modelTokey = null;
        try {
            modelTokey = getObject(Config.CACHE_KEY, ModelTokey.class);
            if (modelTokey == null) {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return modelTokey.getAccountVo().getAid();
    }



    public <T> T getObject(String key, Class<T> classOfT) throws IOException, ClassNotFoundException {
        if (mSharedPreferences == null) {
            return null;
        }
        if (mSharedPreferences.contains(key)) {
            String string = mSharedPreferences.getString(key, "");
            if (TextUtils.isEmpty(string)) {
                return null;
            } else {
                try {
                    new JSONObject(string);
                    return mGson.fromJson(string, classOfT);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return getSerializableObject(string);
                }
            }
        }
        return null;
    }

    /**
     * 从本地反序列化获取对象
     *
     * @return
     */
    private <T> T getSerializableObject(String content) throws IOException, ClassNotFoundException {
        try {
            if (TextUtils.isEmpty(content)) {
                return null;
            } else {
                //将16进制的数据转为数组，准备反序列化
                byte[] stringToBytes = StringToBytes(content);
                ByteArrayInputStream bis = new ByteArrayInputStream(stringToBytes);
                ObjectInputStream is = new ObjectInputStream(bis);
                //返回反序列化得到的对象
                T readObject = (T) is.readObject();
                return readObject;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * desc:将16进制的数据转为数组
     *
     * @param data
     * @return
     */
    byte[] StringToBytes(String data) {
        String hexString = data.toUpperCase().trim();
        if (hexString.length() % 2 != 0) {
            return null;
        }
        byte[] retData = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i++) {
            int int_ch;  // 两位16进制数转化后的10进制数
            char hex_char1 = hexString.charAt(i); //两位16进制数中的第一位(高位*16)
            int int_ch1;
            if (hex_char1 >= '0' && hex_char1 <= '9')
                int_ch1 = (hex_char1 - 48) * 16;   // 0 的Ascll - 48
            else if (hex_char1 >= 'A' && hex_char1 <= 'F')
                int_ch1 = (hex_char1 - 55) * 16; // A 的Ascll - 65
            else
                return null;
            i++;
            char hex_char2 = hexString.charAt(i); //两位16进制数中的第二位(低位)
            int int_ch2;
            if (hex_char2 >= '0' && hex_char2 <= '9')
                int_ch2 = (hex_char2 - 48); // 0 的Ascll - 48
            else if (hex_char2 >= 'A' && hex_char2 <= 'F')
                int_ch2 = hex_char2 - 55; // A 的Ascll - 65
            else
                return null;
            int_ch = int_ch1 + int_ch2;
            retData[i / 2] = (byte) int_ch;//将转化后的数放入Byte里
        }
        return retData;
    }


    public void setContext(Activity context) {
        this.context = context;
    }
}
