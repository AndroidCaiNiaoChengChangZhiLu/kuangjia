package com.tima.api.been;

import java.lang.reflect.Type;

/**
 * Created by Administrator on 2017/7/10.
 */

public class ModelBase<T>{
    private String name;
    private Type classType;

    public Type getClassType() {
        return classType;
    }

    public ModelBase setClassType(Type classType) {
        this.classType = classType;
        return this;
    }

    public ModelBase setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }
}
