package com.tima.api.been;

import java.io.Serializable;

/**
 * Created by MrYuan on 2017/5/3.
 */

public class ModelTokey implements Serializable {

    private AccountVoBean accountVo;
    private String status;
    private String token;
    /**
     * 错误码
     */
    public String errorCode;

    /**
     * 错误信息
     */
    public String errorMessage;

    @Override
    public String toString() {
        return "ModelTokey{" +
                "status='" + status + '\'' +
                ", token='" + token + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public AccountVoBean getAccountVo() {
        return accountVo;
    }

    public void setAccountVo(AccountVoBean accountVo) {
        this.accountVo = accountVo;
    }

    public static class AccountVoBean implements Serializable {
        /**
         * "aid": "22",
         * "idType": "IDCARD",
         * "idNumber": "511010199202201234",
         * "mobile": "18202101366",
         * "nickName": "徐东",
         * "accType": "CAR_OWNER",
         * "headPicPath": "http://fawmcdev.timanetwork.com/rs//5732477868user/headPicPath/dc988931f3bd4fecb1b43be7de610362.jpg",
         * "sex": "F",
         * "name": "徐东"
         */

        private String sex;
        private String headPicPath;
        private String aid;
        private String idType;
        private String idNumber;
        private String mobile;
        private String nickName;
        private String name;
        private String accType;


        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getHeadPicPath() {
            return headPicPath;
        }

        public void setHeadPicPath(String headPicPath) {
            this.headPicPath = headPicPath;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public String getIdType() {
            return idType;
        }

        public void setIdType(String idType) {
            this.idType = idType;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAccType() {
            return accType;
        }

        public void setAccType(String accType) {
            this.accType = accType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }
    }
}
