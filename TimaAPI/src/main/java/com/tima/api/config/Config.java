package com.tima.api.config;


import android.content.Context;

/**
 * 公共参数的配置.
 *
 * @author Jacob.Wu
 */
public class Config {

    public static int TOKEN_MODE = Context.MODE_WORLD_READABLE;
    /**
     * 从车机共享数据库中获取权限数据
     */
    public static final String CACHE_PACKAGE = "attestation.tima.com.Attestation";   //获取token应用包名
    public static final String CACHE_NAME = "carcache";                                //获取token储存文件名
    public static final String CACHE_KEY = "cartoken";                                  //获取token储存文件key

    public static String Appkey;
}
